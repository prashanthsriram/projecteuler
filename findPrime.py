# Find the 10001st prime

import math

def isPrime(n):
    if n<2:
        return False
    else:
        for i in range(2, int(math.sqrt(n)) + 1):
            if n%i == 0:
                return False
        
        return True
        

def find_nth_prime(n):
    count = 1
    i = 2
    while count <= n:
        if isPrime(i):
            count += 1
        i += 1
    
    return i-1


if __name__ == "__main__":
    print find_nth_prime(10001) 

# Find the smallest positive number that is evenly divisible by all of the numbers from 1 to 20

def gcd(a, b):
    if b > a:
        a, b = b, a
    
    while a%b != 0:
        a, b = b, a%b
    
    return b   
    

def smallestMul(n):
    mul = 1    
    for i in range(2, n+1):
        mul = mul*i / gcd(mul, i)
    
    return mul


if __name__ == "__main__":
    print smallestMul(20)

# Finding the largest prime factor of a number

import math

def largestPrime(n):
    largest = 0
    for i in range(2, int(math.sqrt(n)) + 1):
        if n%i == 0:
            largest = i
            while n%i == 0:
                n /= i
    
    return largest


if __name__ == "__main__":
    print largestPrime(600851475143)

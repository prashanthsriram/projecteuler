def ssd():
    a = sum(range(1, 101))
    b = sum([i*i for i in range(1, 101)])
    return a*a - b


if __name__ == "__main__":
    print ssd()

# Find the largest pallindrome product of 2 3-digit numbers

def largestPallProd(n):
    return max(i*j for i in range(10**(n-1), 10**n) for j in range(10**(n-1), 10**n) if str(i*j) == str(i*j)[::-1])


if __name__ == "__main__":
    print largestPallProd(3)

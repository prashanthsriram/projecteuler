# Find the product abc where a,b,c are pythagorean triplets and a+b+c = 1000 (only 1 such triplet exists)

def pyTrip():
    for i in range(1, 1000):
        for j in range(i+1, 1000):
            for k in range(j+1, 1000):
                if i*i + j*j == k*k:
                    if i+j+k == 1000:
                        return i,j,k,i*j*k


if __name__ == "__main__":
    print pyTrip()

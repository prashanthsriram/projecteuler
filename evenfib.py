def evenfib():
    i, j, k, tot = 1, 2, 0, 2
    while k <= 4000000:
        k = i+j
        i, j = j, k
        if k%2 == 0:
            tot += k
    
    return tot


if __name__ == "__main__":
    print evenfib()

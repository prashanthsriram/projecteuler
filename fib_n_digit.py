# Find the first 1000 digit fibonacci number and print

def fib_n_digit(n):
    a, b, t = 1, 1, 2
    while len(str(b)) < n:
        b, a = a+b, b
        t += 1

    return t

if __name__ == "__main__":
    print fib_n_digit(1000)
